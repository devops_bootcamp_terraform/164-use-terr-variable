
provider "aws" {}
variable cidrBlock_eks {
  type        = list(string)
 
  description = "the cidr block of eks subnet"
}
variable cidrBlock_ec2 {
  type        = list(object({
    cidr_bloc = string
    name = string
  }))
  description = "the cidr block of eks subnet"
}
variable availb_zone {}

variable "environment" {
    description = "the environment of deployment"
}
####### how to use variable with type list ######
resource "aws_vpc" "eksvpc" {
     cidr_block = var.cidrBlock_eks[0]
    tags = {
        Name = var.environment
     }
}
resource "aws_subnet" "ekssubnet" {
    vpc_id = aws_vpc.eksvpc.id
    cidr_block = var.cidrBlock_eks[1]
    availability_zone = var.availb_zone
    tags = {
        Name = var.environment
     }
}
####### how to use variable with type list of object ######
resource "aws_vpc" "ec2vpc" {
     cidr_block = var.cidrBlock_ec2[0].cidr_bloc
    tags = {
        Name = var.cidrBlock_ec2[0].name
     }
}
resource "aws_subnet" "ec2subnet" {
    vpc_id = aws_vpc.ec2vpc.id
    cidr_block = var.cidrBlock_ec2[1].cidr_bloc
    availability_zone = "us-east-1a"
    tags = {
        Name = var.cidrBlock_ec2[1].name
     }
}
data "aws_vpc" "defaultvpc" {
    default = true
}
resource "aws_subnet" "subnet_def_vpc" {
    vpc_id = data.aws_vpc.defaultvpc.id
    cidr_block = "172.31.240.0/20"
    availability_zone = "us-east-1b"
}
output "eks-vpc-id" {
    value = aws_vpc.eksvpc.id
}
output "eks-subnet-id" {
    value = aws_subnet.ekssubnet.id
}
